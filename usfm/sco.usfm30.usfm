\id AMO
\toc1
\toc2
\toc3
\c 1
\s Book Introduction - Amos
\p @@reference osisRef="am 1"µµRead first chapter of Amos@@/referenceµµ
\p Amos, a Jew, but prophesying (B.C. 776-763) in the northern kingdom (@@reference osisRef="am 1:1"µµAmos 1:1@@/referenceµµ; @@reference osisRef="am 7:14 am 7:15"µµ 7:14,15@@/referenceµµ) exercised his ministry during the reign of Jeroboam II, an able but idolatrous king who brought his kingdom to the zenith of its power. Nothing could seem more improbable than the fulfilment of Amos' warnings; yet within fifty years the kingdom was utterly destroyed. The vision of Amos is, however, wider than the northern kingdom, including the whole "house of Jacob."
\p Amos is in four parts:
\li Judgments on the cities surrounding Palestine, 1:1-2:3.
\li Judgements on Judah and Israel, 2:4-16.
\li Jehovah's controversy with "the whole family" of Jacob, 3:1-9:10.
\li The future glory of the Davidic kingdom, 9:11-15.
\p
\v 2 \bd roar\bd*
\p "Roar," etc. Cf. @@reference osisRef="isa 42:13"µµIsaiah 42:13@@/referenceµµ; @@reference osisRef="jer 25:30-33"µµJeremiah 25:30-33@@/referenceµµ; @@reference osisRef="ho 11:10 ho 11:11"µµHosea 11:10,11@@/referenceµµ; @@reference osisRef="joe 3:16"µµJoel 3:16@@/referenceµµ. It will be found that wherever the phrase occurs it is connected with the destruction of Gentile dominion (see "Times of the Gentiles," @@reference osisRef="lu 21:24"µµLuke 21:24@@/referenceµµ. \it (See Scofield "@@reference osisRef="Scofield:Re 16:19"µµRevelation 16:19@@/referenceµµ")\it* and the blessing of Israel in the kingdom. Without a doubt a near fulfilment upon Syria occurred @@reference osisRef="2ki 14:28"µµ2 Kings 14:28@@/referenceµµ but the expression, "the Lord will roar," looks forward to a vaster fulfilment. See Scofield "@@reference osisRef="Scofield:joe 1:4"µµJoel 1:4@@/referenceµµ".
\c 2
\p
\v 1
\v 4 \bd For three\bd*
\p The judgments on Judah and Israel were fulfilled as to Judah in the 70 years' captivity; as to Israel (the northern kingdom) in the world-wide dispersion which still continues.
\p
\c 3
\p
\v 1 \bd whole family\bd*
\p The language here, and the expression "house of Jacob," @@reference osisRef="am 3:13"µµAmos 3:13@@/referenceµµ evidently gives the prophecy a wider application than to "Israel," the ten-tribe northern kingdom, though the judgment was, in the event, executed first upon the northern kingdom. @@reference osisRef="2ki 17:18-23"µµ2 Kings 17:18-23@@/referenceµµ.
\v 2 \bd therefore\bd*
\p It is noteworthy that Jehovah's controversy with the Gentile cities which hated Israel is brief: "I will send a fire." But Israel had been brought into the place of privilege and so of responsibility, and the Lord's indictment is detailed and unsparing. Cf. @@reference osisRef="mt 11:23"µµMatthew 11:23@@/referenceµµ; @@reference osisRef="lu 12:47 lu 12:48"µµLuke 12:47,48@@/referenceµµ.
\c 4
\p
\v 1
\v 4 \bd Beth-el\bd*
\p Cf. @@reference osisRef="1ki 12:25-33"µµ1 Kings 12:25-33@@/referenceµµ. Any altar at Beth-el, after the establishment of Jehovah's worship at Jerusalem was of necessity divisive and schismatic. @@reference osisRef="de 12:4-14"µµDeuteronomy 12:4-14@@/referenceµµ. Cf. ; @@reference osisRef="joh 4:21-24"µµJohn 4:21-24@@/referenceµµ; @@reference osisRef="mt 18:20"µµMatthew 18:20@@/referenceµµ; @@reference osisRef="heb 13:10-14"µµHebrews 13:10-14@@/referenceµµ.
\v 5 \bd thanksgiving\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Le 7:13"µµLeviticus 7:13@@/referenceµµ")\it*.
\c 5
\p
\v 1
\v 15 \bd remnant\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Jer 15:21"µµJeremiah 15:21@@/referenceµµ")\it*.
\p
\c 6
\p
\v 1 \bd trust\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 2:12"µµPsalms 2:12@@/referenceµµ")\it*.
\c 7
\p
\v 1
\v 3 \bd repented\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Zec 8:14"µµZechariah 8:14@@/referenceµµ")\it*.
\v 8 \bd plumbline\bd*
\p Symbol of judgment according to righteousness.
\v 9 \bd raise\bd*
\p As prophesied and fulfilled, @@reference osisRef="2ki 15:10"µµ2 Kings 15:10@@/referenceµµ.
\p
\c 9
\p
\v 1 \bd standing\bd*
\p The position of the Lord (Adonai) is significant. The altar speaks properly of mercy because of judgement executed upon an interposed sacrifice, but when altar and sacrifice are despised the altar becomes a place of judgment. Cf. @@reference osisRef="joh 12:31"µµJohn 12:31@@/referenceµµ.
\v 2 \bd hell\bd*
\p Heb. "Sheol," \it (See Scofield "@@reference osisRef="Scofield:Hab 2:5"µµHabakkuk 2:5@@/referenceµµ")\it*.
\v 9 \bd sift\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 72:1"µµPsalms 72:1@@/referenceµµ")\it*.
