\id 1CH
\toc1
\toc2
\toc3
\c 1
\s Book Introduction - 1 Chronicles
\p @@reference osisRef="1ch 1"µµRead first chapter of 1 Chronicles@@/referenceµµ
\p The two books of Chronicles (like the two books of Kings) are but one book in the Jewish canon. Together they cover the period from the death of Saul to the captivities. They were written probably during the Babylonian captivity, and are distinguished from the two books of the Kings in a fuller account of Judah, and in the omission of many details. The blessing of God's earthly people in connection with the Davidic monarchy is probably the typical significance of these books.
\p First Chronicles is in three parts:
\li Official genealogies, 1:1-9:44.
\li From the death of Saul to the accession of David, 10:1-12:24.
\li From the accession of David to his death, 13:1-29:30.
\p Excluding the genealogies (Ch 1-9) the events recorded in First Chronicles cover a period of 41 years (Ussher).
\v 5 \bd Magog\bd*
\p @@reference osisRef="ge 10:2"µµGenesis 10:2@@/referenceµµ; @@reference osisRef="eze 38:2"µµEzekiel 38:2@@/referenceµµ; @@reference osisRef="eze 39:6"µµ 39:6@@/referenceµµ; @@reference osisRef="re 20:8"µµRevelation 20:8@@/referenceµµ. \it (See Scofield "@@reference osisRef="Scofield:Eze 38:2"µµEzekiel 38:2@@/referenceµµ")\it*.
\c 2
\p
\v 1
\v 13 \bd Shimma\bd*
\p Or, Shammah, @@reference osisRef="1sa 16:9"µµ1 Samuel 16:9@@/referenceµµ.
\v 54 \bd Ataroth\bd*
\p Or, Atarites, or, crowns of the house of Joab.
\c 3
\p
\v 1
\v 2 \bd Absalom\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:2Sa 13:37"µµ2 Samuel 13:37@@/referenceµµ")\it*.
\c 5
\p
\v 1
\v 6 \bd Tilgathpilneser\bd*
\p Or, Tiglathpileser, @@reference osisRef="2ki 15:29"µµ2 Kings 15:29@@/referenceµµ; @@reference osisRef="2ki 16:7"µµ 16:7@@/referenceµµ.
\v 20 \bd trust\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 2:12"µµPsalms 2:12@@/referenceµµ")\it*.
\c 6
\p
\v 1
\v 68 \bd Jokmeam\bd*
\p See @@reference osisRef="jos 21:22-35"µµJoshua 21:22-35@@/referenceµµ where many of these cities have other names:
\c 7
\p
\v 1
\v 12 \bd Shuppim\bd*
\p Shupham and Hupham. @@reference osisRef="nu 26:39"µµNumbers 26:39@@/referenceµµ.
\v 28 \bd Narran\bd*
\p Naarath, @@reference osisRef="jos 16:7"µµJoshua 16:7@@/referenceµµ.
\c 9
\p
\v 1
\v 26 \bd chambers\bd*
\p i.e. storehouses.
\v 32 \bd shewbread\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 25:30"µµExodus 25:30@@/referenceµµ")\it*.
\v 41 \bd and Ahaz\bd*
\p Added from @@reference osisRef="1ch 8:35"µµ1 Chronicles 8:35@@/referenceµµ.
\c 11
\p
\v 1
\v 5 \bd castle of Zion\bd*
\p Heb. castle.
\li (1) Zion, the ancient Jebusite stronghold, is the southwest eminence in Jerusalem, called in Scripture the city of David, and associated with the Davidic royalty both historically and prophetically @@reference osisRef="1ch 11:7"µµ1 Chronicles 11:7@@/referenceµµ; @@reference osisRef="ps 2:6"µµPsalms 2:6@@/referenceµµ; @@reference osisRef="isa 2:3"µµIsaiah 2:3@@/referenceµµ. The word is often used of the whole city of Jerusalem considered as the city of God @@reference osisRef="ps 48:2 ps 48:3"µµPsalms 48:2,3@@/referenceµµ especially in passages referring to the future kingdom-age ; @@reference osisRef="isa 1:27"µµIsaiah 1:27@@/referenceµµ; @@reference osisRef="isa 2:3"µµ 2:3@@/referenceµµ; @@reference osisRef="isa 4:1-6"µµ 4:1-6@@/referenceµµ; @@reference osisRef="joe 3:16"µµJoel 3:16@@/referenceµµ; @@reference osisRef="zec 1:16 zec 1:17"µµZechariah 1:16,17@@/referenceµµ; @@reference osisRef="zec 8:3-8"µµ 8:3-8@@/referenceµµ; @@reference osisRef="ro 11:26"µµRomans 11:26@@/referenceµµ. In @@reference osisRef="heb 12:22"µµHebrews 12:22@@/referenceµµ the word is used symbolically of heaven.
\li (2) In @@reference osisRef="de 4:48"µµDeuteronomy 4:48@@/referenceµµ the name is given to a projection or peak of Mount Hermon.
\c 12
\p
\v 1
\v 15 \bd first month\bd*
\p i.e. April.
\c 13
\p
\v 1
\v 11 \bd Perezuzza\bd*
\p i.e. the breach of Uzza.
\c 16
\p
\v 1
\v 4 \bd record\bd*
\p See titles of Psalms 38 and 70.
\v 16 \bd Abraham\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ge 15:18"µµGenesis 15:18@@/referenceµµ")\it*.
\v 25 \bd feared\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\v 37

\p It will be understood that the ancient tabernacle was now divided; the ark was brought into "Zion." \it (See Scofield "@@reference osisRef="Scofield:1Ch 11:5"µµ1 Chronicles 11:5@@/referenceµµ")\it*.
\c 18
\p
\v 1
\v 8 \bd Tibhath\bd*
\p Called Betah, and Berothai. @@reference osisRef="2sa 8:8"µµ2 Samuel 8:8@@/referenceµµ.
\v 12 \bd Abishai\bd*
\p Nephew of David, brother to Joab. @@reference osisRef="2sa 23:18"µµ2 Samuel 23:18@@/referenceµµ; @@reference osisRef="1ch 2:16"µµ1 Chronicles 2:16@@/referenceµµ.
\p
\c 20
\p
\v 1 \bd David\bd*
\p Here should be read @@reference osisRef="2sa 11:2-2sa 12:25"µµ2 Samuel 11:2-12:25@@/referenceµµ; @@reference osisRef="ps 51:1-19"µµPsalms 51:1-19@@/referenceµµ.
\c 21
\p
\v 1
\v 4 \bd departed\bd*
\p Here should read @@reference osisRef="2sa 24:4-9"µµ2 Samuel 24:4-9@@/referenceµµ.
\v 5 \bd were a thousand\bd*
\p Cf. \it (See Scofield "@@reference osisRef="Scofield:2Sa 24:9"µµ2 Samuel 24:9@@/referenceµµ")\it*.
\v 12

\p Marg
\p \it (See Scofield "@@reference osisRef="Scofield:Heb 1:4"µµHebrews 1:4@@/referenceµµ")\it*.
\v 15 \bd angel\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Heb 1:4"µµHebrews 1:4@@/referenceµµ")\it*.
\v 18 \bd angel\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Heb 1:4"µµHebrews 1:4@@/referenceµµ")\it*.
\v 20 \bd angel\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Heb 1:4"µµHebrews 1:4@@/referenceµµ")\it*.
\v 25 \bd six hundred shekels of gold\bd*
\p A discrepancy has been imagined in the two accounts, @@reference osisRef="2sa 24:24"µµ2 Samuel 24:24,@@/referenceµµ; @@reference osisRef="1ch 21:25"µµ1 Chronicles 21:25@@/referenceµµ; @@reference osisRef="2sa 24:24"µµ2 Samuel 24:24@@/referenceµµ records the price of the threshingfloor (heb. goren); @@reference osisRef="1ch 21:25"µµ1 Chronicles 21:25@@/referenceµµ of the place (Heb. magom, lit. "home," @@reference osisRef="1sa 2:20"µµ1 Samuel 2:20@@/referenceµµ) same word or area on which afterward the great temple, with its spacious courts was built. @@reference osisRef="2ch 3:1"µµ2 Chronicles 3:1@@/referenceµµ.
\p David gave fifty shekels of sliver for the "goren"; six hundred shekels of gold for the "magom."
\v 27 \bd angel\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Heb 1:4"µµHebrews 1:4@@/referenceµµ")\it*.
\c 23
\p
\v 1
\v 22 \bd brethren\bd*
\p i.e. cousins.
\v 28 \bd office\bd*
\p i.e. their new office, since their former office of bearing the tabernacle was ended.
\v 29 \bd shewbread\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 25:30"µµExodus 25:30@@/referenceµµ")\it*.
\c 27
\p
\v 1
\v 2 \bd first month\bd*
\p i.e. April; @@reference osisRef="1ch 27:3"µµ1 Chronicles 27:3@@/referenceµµ.
\v 4 \bd second month\bd*
\p i.e. May.
\v 5 \bd third month\bd*
\p i.e. June.
\v 7 \bd fourth month\bd*
\p i.e. July.
\v 8 \bd fifth month\bd*
\p i.e. August.
\v 9 \bd sixth month\bd*
\p i.e. September.
\v 10 \bd seventh month\bd*
\p i.e. October.
\v 11 \bd eighth month\bd*
\p i.e. November.
\v 12 \bd ninth month\bd*
\p i.e. December.
\v 13 \bd tenth month\bd*
\p i.e. January.
\v 14 \bd eleventh month\bd*
\p i.e. February.
\v 15 \bd twelfth month\bd*
\p i.e. March.
\c 28
\p
\v 1
\v 16 \bd shewbread\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 25:30"µµExodus 25:30@@/referenceµµ")\it*.
\c 29
\p
\v 1
\v 10 \bd blessed\bd*
\p Note the order:
\p @@reference osisRef="1ch 29:3-8"µµ1 Chronicles 29:3-8,@@/referenceµµ giving, @@reference osisRef="1ch 29:9"µµ1 Chronicles 29:9,@@/referenceµµ joy, @@reference osisRef="1ch 29:10"µµ1 Chronicles 29:10,@@/referenceµµ blessing, @@reference osisRef="1ch 29:11-19"µµ1 Chronicles 29:11-19,@@/referenceµµ prayer, @@reference osisRef="1ch 29:20"µµ1 Chronicles 29:20,@@/referenceµµ worship:
\v 29 \bd book of\bd*
\p These books have perished.
